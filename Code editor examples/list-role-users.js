let authApi = new platformClient.AuthorizationApi();

function getRoles() {
  let opts = {
    pageSize: 500
  };
  
  console.log("Getting roles");
  
  authApi.getAuthorizationRoles(opts).then((data) => {
    var roleIds = [];
    
    data.entities.forEach((entity) => {
      roleIds.push(entity.id);
    });
    
    console.log(`Got ${roleIds.length} roles`);
    
    getRoleUsers(roleIds);
  });
}

function getRoleUsers(roleIds) {
  let opts = {
    pageSize: 500
  };
  
  roleIds.forEach((roleId) => {
    var roleAssignments = [];
    
    authApi.getAuthorizationRoleUsers(roleId, opts).then((data) => {
      data.entities.forEach((user) => {
        roleAssignments.push(user.id);
      });
      
      console.log(`User assignments for role ${roleId}:`, JSON.stringify(roleAssignments));
    });
  });
}

getRoles();