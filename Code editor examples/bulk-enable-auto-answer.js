let usersApi = new platformClient.UsersApi();
var userIds = [];

function getUsers(pageNumber) {
  let opts = {
	'pageSize': 500,
	'pageNumber': pageNumber
  };
  
  usersApi.getUsers(opts).then((data) => {
	data.entities.forEach((entity) => {
	  userIds.push(entity.id);
	});
	
	if (data.pageNumber < data.pageCount) {
	  getUsers(data.pageNumber + 1);
	} else {
	  updateUsers();
	}
  });
}

function updateUsers() {
  var userObjects = [];
  var needsUpdated;
  
  userIds.forEach((id) => {
	var user = {
	  'id': id,
	  'acdAutoAnswer': true
	};
	userObjects.push(user);
	
	if (userObjects.length == 50) {
	  let userObjectsSend = userObjects.slice();
	  userObjects = [];
	  needsUpdated = false;
	  performBulkUpdate(userObjectsSend);
	} else {
	  needsUpdated = true;
	}
	
  });
  
  if (needsUpdated) performBulkUpdate(userObjects);
}

function performBulkUpdate(userObjects) {
  usersApi.patchUsersBulk(userObjects).then((data) => {
	console.log("Updated users:", userObjects);
  });
}

getUsers(1);