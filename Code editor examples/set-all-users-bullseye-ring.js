// List of queue IDs to update all members for
const queues = [
  "e25bba75-aaaa-aaaa-aaaa-f3d7902566f7",
  "732e1d5e-aaaa-aaaa-aaaa-295edcb124b0",
];
const ring = 6; // Bullseye ring to set all members of the above queues to

let routingApi = new platformClient.RoutingApi();

function updateQueue(queueIndex) {
  getQueueMembers(queueIndex, [], 1);
}

function getQueueMembers(queueIndex, members, pageNumber) {
  let opts = {
    pageSize: 100,
    pageNumber: pageNumber,
  };

  console.log(
    `Getting member page ${pageNumber} of queue ${queues[queueIndex]}`
  );

  routingApi
    .getRoutingQueueMembers(queues[queueIndex], opts)
    .then((data) => {
      data.entities.forEach((entity) => {
        members.push(entity.id);
      });

      if (data.entities.length >= opts.pageSize) {
        // get next page
        getQueueMembers(queueIndex, members, data.pageNumber + 1);
      } else if (members.length > 0) {
        // update the members of this queue
        setQueueMemberRing(queueIndex, members, 0);
      } else {
        console.log("No members found in queue " + queues[queueIndex]);

        // move on to the next queue if necessary.
        if (queues.length > queueIndex + 1) {
          updateQueue(queueIndex + 1);
        } else {
          console.log("Done!");
        }
      }
    })
    .catch((err) => {
      if (err.status == "429") {
        var retryAfter = 60;
        console.log("Rate limited, waiting " + retryAfter);

        setTimeout(() => {
          // retry this page
          getQueueMembers(queueIndex, members, pageNumber);
        }, retryAfter * 1000);
      } else {
        console.log(
          `Error getting members for queue ${queues[queueIndex]}`,
          err
        );
      }
    });
}

function setQueueMemberRing(queueIndex, members, memberIndex) {
  routingApi
    .patchRoutingQueueMember(queues[queueIndex], members[memberIndex], {
      ringNumber: ring,
    })
    .then((data) => {
      console.log(
        `${memberIndex} - Updated user ${members[memberIndex]} to ring ${ring} for queue ${queues[queueIndex]}`
      );

      if (members.length > memberIndex + 1) {
        setQueueMemberRing(queueIndex, members, memberIndex + 1);
      } else {
        // this means we have exhausted all members in this queue.
        // move on to the next queue if necessary.
        if (queues.length > queueIndex + 1) {
          updateQueue(queueIndex + 1);
        } else {
          console.log("Done!");
        }
      }
    })
    .catch((err) => {
      if (err.status == "429") {
        var retryAfter = 60;
        console.log("Rate limited, waiting " + retryAfter);

        setTimeout(() => {
          // retry this queue member
          setQueueMemberRing(queueIndex, members, memberIndex);
        }, retryAfter * 1000);
      } else {
        console.log(
          `Error updating member ${members[memberIndex]} for queue ${queues[queueIndex]}`,
          err
        );
      }
    });
}

updateQueue(0);
