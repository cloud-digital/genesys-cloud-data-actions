let routingApi = new platformClient.RoutingApi();

function getQueues() {
  let opts = {
    pageSize: 50
  };
  
  console.log("Getting queues");
  
  routingApi.getRoutingQueues(opts).then((data) => {
    var queueIds = [];
    
    data.entities.forEach((entity) => {
      queueIds.push(entity.id);
    });
    
    console.log(`Got ${queueIds.length} queues`);
    
    getQueueMembers(queueIds);
  });
}

function getQueueMembers(queueIds) {
  let opts = {
    pageSize: 500
  };
  queueIds.forEach((queueId) => {
    var queueMembers = [];
    var queueMemberIds = []; // better suited for bulk adding of queue members
    
    routingApi.getRoutingQueueUsers(queueId, opts).then((data) => {
      data.entities.forEach((entity) => {
        var member = {
          id: entity.id,
          name: entity.name,
          ringNumber: entity.ringNumber,
          joined: entity.joined
        };
        
        queueMembers.push(member);
        queueMemberIds.push(entity.id);
      });
      
      console.log(`Members for queue ${queueId}:`, JSON.stringify(queueMembers));
      console.log(`Member IDs for queue ${queueId}:`, JSON.stringify(queueMemberIds));
    });
  });
}

getQueues();