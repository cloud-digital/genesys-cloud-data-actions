const skill = "6b76c866-aaaa-aaaa-aaaa-75866e2723f0"; // Skill ID to add to all users
const proficiency = 1.0; // Proficiency to set for this skill on all users

let usersApi = new platformClient.UsersApi();
var userIds = [];

function getUsers(pageNumber) {
  let opts = {
    pageSize: 500,
    pageNumber: pageNumber,
  };

  usersApi
    .getUsers(opts)
    .then((data) => {
      data.entities.forEach((entity) => {
        userIds.push(entity.id);
      });

      if (data.pageNumber < data.pageCount) {
        getUsers(data.pageNumber + 1);
      } else if (userIds.length > 0) {
        setUserSkill(0, skill, proficiency);
      } else {
        console.log("No users found");
      }
    })
    .catch((err) => {
      console.log("Error fetching users", err);
    });
}

function setUserSkill(index, skillId, prof) {
  usersApi
    .postUserRoutingskills(userIds[index], { id: skillId, proficiency: prof })
    .then((data) => {
      console.log(index + " - Added skill to user " + userIds[index]);

      if (userIds.length > index + 1) {
        setUserSkill(index + 1, skillId, prof);
      }
    })
    .catch((err) => {
      if (err.status == "429") {
        var retryAfter = 60;
        console.log("Rate limited, waiting " + retryAfter);

        setTimeout(() => {
          // retry this user
          setUserSkill(index, skillId, prof);
        }, retryAfter * 1000);
      } else {
        console.log("Error updating user " + userIds[index], err);
      }
    });
}

getUsers(1);
