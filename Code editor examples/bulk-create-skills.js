const skills = ["skill1", "skill2"];

let routingApi = new platformClient.RoutingApi();

function addSkill(skillIndex) {
  routingApi
    .postRoutingSkills({ name: skills[skillIndex] })
    .then((data) => {
      console.log(`Added skill ${skills[skillIndex]}`);

      skillIndex += 1;
      if (skillIndex < skills.length) {
        addSkill(skillIndex);
      }
    })
    .catch((error) => {
      if (error.status == "429") {
        var retryAfter = 60;
        console.log("Rate limited, waiting " + retryAfter);

        setTimeout(() => {
          addSkill(skillIndex);
        }, retryAfter * 1000);
      } else {
        console.log(`Error adding skill ${skills[skillIndex]}`, error);

        // when faced with adversity, power through
        skillIndex += 1;
        if (skillIndex < skills.length) {
          addSkill(skillIndex);
        }
      }
    });
}

addSkill(0);
