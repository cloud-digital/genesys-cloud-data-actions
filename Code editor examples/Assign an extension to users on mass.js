/*The below code allows you to automate the adding of a extension to a users profile. Using this code will currently remove any other
 contact numbers configured on the user (code could be updated to keep them).
 
Use this code in excel to add the comma to your email  
=CHAR(34)&A1&CHAR(34)&","

Use this code in excel to add the comma to your extensions quickly 
=A1&","

Change the pageNumber variable to cycle through more users
*/

let pageSize = 300;

let pageNumber = 1;

//List of users emails you want to update. Case does not matter
var userEmail = [
];

//Extensions of the above emails to apply. Must match order of userEmail
var userExtension = [

];

//API Object creation
let apiInstance = new platformClient.UsersApi();

//Used to determine if primary contact exists already
let primaryExists = false;

//Stores the primary contact number
let primaryNumber = "";

//Stores the userDataObjects into this array
let userDataArray = [];

//Object used to store data on matched email accounts
var userDataObject = {
  "userId": "",
  "userVersion": 0,
  "userExtension": 0,
  "userEmail": "",
  "primaryExists": false,
  "userPrimaryNumber": ""
};

//Object for patching user phone extension
let body = {
  "version": String,
  "primaryContactInfo": {
    "address": String,
    "display": String,
    "mediaType": String,
    "type": String,
    "extension": String,
  },
};

//Used to retrieve users in PureCloud. Increase "pageSize" and/or "pageNumber" to request more users to search against 
let opts = {
  'pageSize': pageSize, // Number | Page size
  'pageNumber': pageNumber, // Number | Page number
  //'id': ["id_example"], // [String] | id
  'sortOrder': "ASC", // String | Ascending or descending sort order
  //'expand': ["expand_example"], // [String] | Which fields, if any, to expand
  'state': "active" // String | Only list users of this state
};

//The below uses the getUsers call followed by patchUser
/*Firstly getUsers is called. Passed in is "opts", "userEmai" and "userExtension". The users retrieved are stored as object "data".
  We then search through the emails returned against "userEmail" for matches. If a match is found then the userId and profile
  version are saved.*/
apiInstance.getUsers(opts)
  .then((data) => {
    console.log("running");
    //Loops through the users retrieved

    for (let i = 0; i < data.entities.length; i++) {
      //Loops through "userEmail" and compares it against the users in "data"

      for (let x = 0; x < userEmail.length; x++) {
        //Checks the email address against each other (sets to lower case). If found assigns version and userId to the variables
        if (data.entities[i].email.toLowerCase() == userEmail[x].toLowerCase()) {
          if (data.entities[i].addresses.length > 0 && data.entities[i].primaryContactInfo[0].hasOwnProperty("address")) {
            primaryNumber = data.entities[i].primaryContactInfo[0].address;
            primaryExists = true;
          }
          userDataObject = {
            "userId": data.entities[i].id,
            "userVersion": data.entities[i].version,
            "userExtension": userExtension[x],
            "userEmail": userEmail[x],
            "primaryExists": primaryExists,
            "userPrimaryNumber": primaryNumber
          };
          userDataArray.push(userDataObject);
          primaryExists = false;
          primaryNumber = true;
          userDataObject = {
            "userId": "",
            "userVersion": "",
            "userExtension": 0,
            "userEmail": "",
            "primaryExists": false,
            "userPrimaryNumber": ""
          };
        }
      }
    }
    if(userDataArray.length === 0){
      console.log("no users found");
    }
    console.log("Found " + userDataArray.length +" matches");
    return userDataArray;
  })
  .catch(function (err) {
    console.log('There was a failure calling getUsers');
    console.error(err);
  })

  /*Second Then
  A loop is used in this "then" to cycle through the found users and create "body" objects for them.
  In the loop we update the "body" object with the primaryNumber (if required), userExtenion number and version number.
  We then pass that to patchUsers and pass in the variables "userId" and "body".*/
  .then((data) => {
    /*updating the body object used in the patchUsers call.
    if user has a primary contact number it will be copied into the body object to be saved
    else just the new extension will be copied into the body object and saved
    */
    
    console.log("--- Assigning Extensions ---");
    
    for (let i = 0; i < userDataArray.length; i++) {
      if (userDataArray[i].userId !== "") {
        if (userDataArray[i].primaryExists === true) {
          body[i] = {
            "addresses": [{
                "address": userDataArray[i].userPrimaryNumber,
                "mediaType": "PHONE",
                "type": "WORK"
              },
              {
                "mediaType": "PHONE",
                "type": "WORK2",
                "extension": userDataArray[i].userExtension
              }
            ],
            "version": userDataArray[i].userVersion
          };
      } else {
          body[i] = {
            "addresses": [{
              "mediaType": "PHONE",
              "type": "WORK2",
              "extension": userDataArray[i].userExtension
            }],
            "version": userDataArray[i].userVersion
          };
        }
      }
    }

    //call to update users extension using the body object created above
    for (let i = 0; i < userDataArray.length; i++) {
       apiInstance.patchUser(userDataArray[i].userId, body[i])
        .then(function (data) {
          console.log("Updated " + userDataArray[i].userEmail + "'s extension to " + userDataArray[i].userExtension);
        })
        .catch(function (err) {
          console.log("**** There was an error updating "+ userDataArray[i].userEmail + " ****");
          console.log(err);
        });
    }

  })
  .catch(function (err) {
    console.log('There was a failure calling patchUser');
    console.error(err);
  });
  
  