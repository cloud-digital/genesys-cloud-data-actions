var usersApi =  new platformClient.UsersApi();

let oldSuffix = "@old-domain.com";
let newSuffix = "@new-domain.com";

let users = [];

let opts = { 
  'pageSize': 100, // Number | Page size
  'pageNumber': 1, // Number | Page number
  'sortOrder': "ASC", // String | Ascending or descending sort order
  'state': "active" // String | Only list users of this state
};

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function processPageOfUsers(results){
  
  for(var x=0; x< results.entities.length; x++){
    let u = results.entities[x];
    users.push(u);
  }

  if(results.nextUri){
    opts.pageNumber++;
    await sleep(250);
    console.log("Getting page...");
    usersApi.getUsers(opts).then(processPageOfUsers);
  }
  else{
    for(var i in users) {
      await sleep(250);
      updateEmail(users[i]);
    }
  }
  return true;
}

function updateEmail(user) {
  console.log("OLD: " + user.id + " " + user.email);
  var newEmail = user.email.replace(oldSuffix, newSuffix);
  usersApi.patchUser(user.id, {"version": user.version, "email": newEmail});
  console.log("New Email: " + newEmail);
}

console.log("Getting page...");
usersApi.getUsers(opts).then(processPageOfUsers);
