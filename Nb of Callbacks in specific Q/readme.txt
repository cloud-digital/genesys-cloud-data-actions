PureCloud Data Action (with example in In-Q flow) for 2 uses:
1/ How many callbacks are already in the specified queue (first use of the Data Action in the In-Q flow)
2/ Does the phone number to be used is already assigned to a callback in the specified queue (second use of the Data Action in the In-Q flow)