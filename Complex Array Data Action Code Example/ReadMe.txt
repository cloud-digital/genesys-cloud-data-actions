In this example, the JSON returned is an array of complex account objects, each of which includes child array of "address". Each object in the address array includes members "addr_type" and a "zip_cd". In this data set, each account can have multiple addresses, and the address array has at least one unique object with addr_type of either "SERVICE" or "BILLING", but maybe one object of each in the array both. 

The desired results needs to have a list of accounts, and a list of the zip_cd for each account. Since we can't flatten a many-to-one relationship of zips<->accounts, we have to create data new structures that are flattened. Since we know that address can only have objects of either addr_type BILLING or SERVICE, even though each is optional, we create lists of address data for each of those types. To ensure we maintain account references for each address, each of these lists must have a unique referecence list of accounts. 

In the accompanying RawResultsSample.json, Data Actions can't create flattened output lists of address, since it would be a list of lists. In the second account in the object array, the following addresses exist:

*******
    "address": [
      {
        "addr_type": "SERVICE",
        "apt_num": "",
        "street_num": "356",
        "street_name": "NAUGHRIGHT RD",
        "street_name_ln2": "",
        "city": "LONG VALLEY",
        "state": "NJ",
        "zip_cd": "07853380656"
      },
      {
        "addr_type": "BILLING",
        "apt_num": "",
        "street_num": "356",
        "street_name": "NAUGHRIGHT RD",
        "street_name_ln2": "",
        "city": "LONG VALLEY",
        "state": "NJ",
        "zip_cd": "07853380656"
      }
    ]
*******

If this array was in the top level of the JSON, Data Actions could easily flatten this into lists of each of the below...

    address.addr_type
    address.zip_cd
    address.state
    etc

...but this would only work for the 2nd account. What about the addresses in the 1st and 3rd accounts? If it were possible, we would try to create an array of ALL the addresses across all accounts, and use manipulation tools to create a matching

    address.acct_num

To ensure we know to which account number each address belongs. However, since JSONPath tools (https://support.smartbear.com/alertsite/docs/monitors/api/endpoint/jsonpath.html) don't have a "get parent" operator to support this. So, what we CAN do is create flattened lists of addresses by their unique addr_type, since the provider has told us there is a unique address per addr_type, per account. The result we can produce is a list of ALL the address data across ALL accounts including the respective acct_num, using a naming convention to specify that they are of addr_type "BILLING" or "SERVICE". The result is the below lists...

    accounts_Billing - an ordered list of acct_num, whose parent object has a member of the address array of objects that contains addr_type "BILLING". In other words, if the account has "BILLING" zip_cd, add the acct_num to this list... in order. If the account does not have a BILLING address, skip it.

    zips_Billing - an ordered list of zip_cd for address objects that contains addr_type "BILLING" accross all account objects. These should appear in the same order as their respective parent acct_num in the accounts_Billing list.

    accounts_Service - an ordered list of acct_num, whose parent object has a member of the address array of objects that contains addr_type "SERVICE". In other words, if the account has "SERVICE" zip_cd, add the acct_num to this list... in order. If the account does not have a SERVICE address, skip it.

    zips_Service - an ordered list of zip_cd for address objects that contains addr_type "SERVICE" accross all account objects. These should appear in the same order as their respective parent acct_num in the accounts_Service list.

A consequence of the above is that we have some redundancy: if an account has both a BILLING and SERVICE address, it will appear in both lists. However, the advantage of the above output schema is that we can have BILLING and SERVICE lists of different sizes. The raw JSON output from the Data Action translation looks like this:
    {
        "accounts_All": [
            "80001576",
            "80000796",
            "80001112"
        ],
        "zips_Billing": [
            "07853380656"
        ],
        "accounts_Billing": [        
            "80000796"
        ],
        "zips_Service": [
            "60150870702",
            "07853380656",
            "39503616831"
        ],
        "accounts_Service": [
            "80001576",
            "80000796",
            "80001112"
        ]
    }

Which means Architect can use loops to account for every Billing and/or Service zip across each account. If there are no zip_cd of an addr_type across all accounts, then the corresponding lists will be empty - the translation JSON output simply won't contain the respective zips/accounts array, and Architect will see this as a collection of size 0.

The translation map works like this:

    "accounts_Service": "$[?(@.address[?(@.addr_type=='SERVICE')] empty false)].acct_num"
    "zips_Service":"$..address[?(@.addr_type=='SERVICE')].zip_cd"

Note the JSONPath notation below

    address[?(@.addr_type=='SERVICE')]

 The square brackets with a ? - [?(criteria)] - means "filter the parent object/array on my left to contain only objects that match this criteria". The @ symbol means "the object I'm filtering". So, if we have a list of address within a single account, this above notation would create a new list of address objects that must contain an addr_type of "SERVICE". 

To create accounts_Service, this means we need to filter the list of acct_num across all accounts to return only those accounts that contain an address of our desired type. In JSONPath, the $ means the root of the JSON.

    $[?(Only account objects with an addr_type of 'SERVICE')].acct_num

So we use JSONPath notation to replace the plain text above with the search criteria we want. Since "address[?(@.addr_type=='SERVICE')]" returns a filtered address[] list, and we simply want to know the accounts where this list has *any* data, we use the list comparison operator "empty"...

    $[?(@.address[?(@.addr_type=='SERVICE')] empty false)].acct_num

For zips_Service, we want the actual zip_cd across each account... but only if it's in an address object with the correct addr_type. In JSONPath, two dots ".." means "get all references across all child objects". Below, we'd get every zip_cd across all accounts/addresses..

    $..zip_cd

But we want..

    $..(Only addresses with addr_type 'SERVICE').zip_cd

So, again we replace with the filtered list of addresses... but across all account objects

    $..address[?(@.addr_type=='SERVICE')].zip_cd

And from there, you have the results described above. After this, we simply repeat for "BILLING" and make sure the successTemplate has somewhere place for each translated output, and matching the output contract of the Data Action
